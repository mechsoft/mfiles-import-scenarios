
## M-Files Harici Kaynaktan Aktarım Senaryoları

### **1. XML ile aktarım**
XML ile aktarım için klasörlerde yer alan dokümanların her birisi için doküman ile aynı isimde bir adet .xml dokümanı yer almalıdır. Dokümanın metadata bilgisi .xml içerisinden okunabilmektedir. Klasör yapısının bir önemi bulunmamaktadır. İç içe klasörler de olabilir.

### **2. CSV veya Excel ile aktarım**
CSV veya Excel ile aktarım da dokümanın metadatası ve filepath ' i aynı satırda yer almalıdır.

| FilePath | Name | CreatedOn | SampleMetadata1 | SampleMetadata2
| -- | --| --| -- | -- |
| \\\uncpath\sample.pdf | Sample Document Name | 2019-09-12 | Mechsoft | Gökay Kıvırcıoğlu

Yukarıda yer alan gibi bir excel veya csv çıktısı alınabilirse dokümanlar M-Files ' a metadata ile birlikte aktarılabilir.